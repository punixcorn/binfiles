## Binaries

Just a bunch of executable? scripts? in c and cpp ?

## Install

added an installer that will **add the folder to path and source the bashfiles for completions** <br/>
**NB** : installer script works if this is cloned in your home folder and you use `zsh` or `bash` <br/>

```bash
cd ~
git clone https://github.com/punixcorn/binfiles
cd ~/binfiles
chmod a+x *
./install.sh
```

or mannually just add the folder to path and source all files in `.completions`

### Source Files

the **source codes** are located in `.src`
`./.completions` holds **bash completions** for binaries

---

### Documentation

> still working on it...

---

### Contributors

<a href="https://github.com/punixcorn/binfiles/graphs/contributors">
  <img src="https://contrib.rocks/image?repo=punixcorn/binfiles" />
</a>
